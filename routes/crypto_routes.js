const Router = require('express');
const crypto_router = new Router();
const CryptoController = require("../controllers/crypto_controller")

crypto_router.get("/listing", CryptoController.listingLatest)
crypto_router.get("/recentlyAdded", CryptoController.recentlyAdded)
crypto_router.get("/gainers", CryptoController.gainers)
crypto_router.get("/losers", CryptoController.losers)
crypto_router.get("/gethdata/:ticker", CryptoController.gethdata)

module.exports = crypto_router