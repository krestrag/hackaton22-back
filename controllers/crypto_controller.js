const axios = require('axios');

class CryptocurrenciesController{
    async listingLatest(req,res){
        
        let response = null;
        new Promise(async (resolve, reject) => {
          try {
            response = await axios.get('https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?convert=RUB', {
              headers: {
                'X-CMC_PRO_API_KEY': 'c0ab3f48-e7e3-46af-9ee1-8852a604585c',
              },
            });
          } catch(ex) {
            response = null;
            console.log(ex);
            reject(ex);
          }
          if (response) {
            var data = []
            response.data.data.forEach(function(object, i){
              data.push({
                number: i+1,
                id: object.id,
                name: object.name,
                symbol: object.symbol,
                price: object.quote.RUB.price.toFixed(2),
                percent_change_24h: object.quote.RUB.percent_change_24h.toPrecision(3) / 100,
                percent_change_7d: object.quote.RUB.percent_change_7d.toPrecision(3)/ 100,
                market_cap:parseInt(object.quote.RUB.market_cap)
              });
            });

            res.json(data);
          }
        });
    }
    async gethdata(req,res){
      let response = null;
      new Promise(async (resolve, reject) => {
        try {
          req.params.ticker
          response = await axios.get('https://min-api.cryptocompare.com/data/v2/histoday?fsym='+req.params.ticker+'&tsym=RUB&limit=100', {
            headers: {
              'Apikey': '32478a4ebf7b218281d6a363fbc64fdc65ce1a29fdc3927d70c35309c97a6db0',
            },
          });
        } catch(ex) {
          response = null;
          console.log(ex);
          reject(ex);
        }
        if (response) {
          var data = []
          response.data.Data.Data.forEach(function(object, i){
            let date = new Date(object.time).toLocaleTimeString("en-US");
            data.push({
              number: i+1,
              time: date,
              price: object.close
            });
          });
          res.json(data);
        }
      });
  }
    async recentlyAdded(req, res){

        let response = null;
        new Promise(async (resolve, reject) => {
          try {
            response = await axios.get('https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?sort=date_added&limit=30&convert=RUB', {
              headers: {
                'X-CMC_PRO_API_KEY': 'c0ab3f48-e7e3-46af-9ee1-8852a604585c',
              },
            });
          } catch(ex) {
            response = null;
            console.log(ex);
            reject(ex);
          }
          if (response) {
            var data = []
            response.data.data.forEach(function(object, i){
              data.push({
                number:i+1,
                id: object.id,
                name: object.name,
                symbol: object.symbol,
                price: object.quote.RUB.price.toFixed(2),
                percent_change_1h: object.quote.RUB.percent_change_1h.toPrecision(3),
                percent_change_24h: object.quote.RUB.percent_change_24h.toPrecision(3),
                market_cap: object.total_supply * object.quote.RUB.price.toPrecision(3),
                volume: object.quote.RUB.volume_24h.toFixed(2)
              });
            });
            res.json(data);
          }
        });
    }
    async gainers(req, res){
        
        let response = null;
        new Promise(async (resolve, reject) => {
          try {
            response = await axios.get('https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?sort=percent_change_24h&limit=20&convert=RUB&sort_dir=desc&volume_24h_min=51000', {
              headers: {
                'X-CMC_PRO_API_KEY': 'c0ab3f48-e7e3-46af-9ee1-8852a604585c',
              },
            });
          } catch(ex) {
            response = null;
            console.log(ex);
            reject(ex);
          }
          if (response) {
          var data = []
          response.data.data.forEach(function(object, i){
              data.push({
                number:i+1,
                id: object.id,
                name: object.name,
                symbol: object.symbol,
                price: object.quote.RUB.price.toFixed(2),
                percent_change_24h: object.quote.RUB.percent_change_24h.toFixed(2),
                volume: object.quote.RUB.volume_24h.toFixed(2)
              });
            });
            res.json(data);
          }
        });
    }
    async losers(req, res){
        
      let response = null;
      new Promise(async (resolve, reject) => {
        try {
          response = await axios.get('https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?sort=percent_change_24h&limit=20&convert=RUB&sort_dir=asc&volume_24h_min=51000', {
            headers: {
              'X-CMC_PRO_API_KEY': 'c0ab3f48-e7e3-46af-9ee1-8852a604585c',
            },
          });
        } catch(ex) {
          response = null;
          console.log(ex);
          reject(ex);
        }
        if (response) {
        var data = []
          response.data.data.forEach(function(object, i){
            data.push({
              number:i+1,
              id: object.id,
              name: object.name,
              price: object.quote.RUB.price.toFixed(2),
              percent_change_24h: object.quote.RUB.percent_change_24h.toFixed(2),
              volume: object.quote.RUB.volume_24h.toFixed(2)
            });
          });
          res.json(data);
        }
      });
  }
}

module.exports = new CryptocurrenciesController()