const db = require('../db');
const config = require('../config');

class WalletController{
    async createUser(req, res) {
        const {user_id, balance_increase, crypto_id} = req.body;
        const checkEmailExists = db.module.query('SELECT  from wallets where email = $1',[email])
        if (checkEmailExists.rowCount > 0){
            res.status(401).json({"status":"failed", "details":"email already registered"});
        }
        const newUser = await db.module.query('INSERT INTO users_wallets (cryptocurrency_value) values ($1) WHERE user_id = $2 and cryptocurrency_id = $3  RETURNING *', [balance_increase, user_id, crypto_id]);
    }

}

module.exports = new UserController();